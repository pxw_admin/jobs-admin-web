
export default {
  // user
  getUser: {},
  getUserById: {},
};

export { default as jobInfo } from './jobInfo'
export { default as jobGroup } from './jobGroup'
export { default as jobLog } from './jobLog'
export { default as jobStatistics } from './jobStatistics'
